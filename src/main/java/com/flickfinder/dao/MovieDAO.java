package com.flickfinder.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.flickfinder.model.Movie;
import com.flickfinder.model.MovieRating;
import com.flickfinder.model.Person;
import com.flickfinder.util.Database;

/**
 * The Data Access Object for the Movie table.
 * 
 * This class is responsible for getting data from the Movies table in the
 * database.
 * 
 */
public class MovieDAO {

	/**
	 * The connection to the database.
	 */
	private final Connection connection;

	/**
	 * Constructs a SQLiteMovieDAO object and gets the database connection.
	 * 
	 */
	public MovieDAO() {
		Database database = Database.getInstance();
		connection = database.getConnection();
	}

	/**
	 * Returns a list of all movies in the database.
	 * 
	 * @return a list of all movies in the database
	 * @throws SQLException if a database error occurs
	 */

	public List<Movie> getAllMovies(String params) throws SQLException {
		List<Movie> movies = new ArrayList<>();

		Statement statement = connection.createStatement();
		String temp = params;
		// I've set the limit to 10 for development purposes - you should do the same.
		if (temp == null) {
			temp = "50";
		}
		ResultSet rs = statement.executeQuery("select * from movies LIMIT " + temp);
		
		while (rs.next()) {
			movies.add(new Movie(rs.getInt("id"), rs.getString("title"), rs.getInt("year")));
		}

		return movies;
	}
	
	public List<Movie> getAllMovies() throws SQLException {
		return getAllMovies(null);
	}

	/**
	 * Returns the movie with the specified id.
	 * 
	 * @param id the id of the movie
	 * @return the movie with the specified id
	 * @throws SQLException if a database error occurs
	 */
	public Movie getMovieById(int id) throws SQLException {

		String statement = "select * from movies where id = ?";
		PreparedStatement ps = connection.prepareStatement(statement);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {

			return new Movie(rs.getInt("id"), rs.getString("title"), rs.getInt("year"));
		}
		
		// return null if the id does not return a movie.

		return null;

	}
	
	public List<Movie> getRatingsByYear(int year, String Limitparam, String Votesparam) throws SQLException {
		// Is this finished?
		List<Movie> movies = new ArrayList<>();
		//String statement = "select * from movies where year = ? order by (select rating from ratings where movie_id = (select id from movies where year = ?)) asc";
		String statement = "select * from movies inner join ratings on movies.id = ratings.movie_id where movies.year = ? AND ratings.votes > " + Votesparam + " ORDER BY ratings.rating DESC LIMIT " + Limitparam;
		PreparedStatement ps = connection.prepareStatement(statement);
		ps.setInt(1, year);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			movies.add(new MovieRating(rs.getInt("id"), rs.getString("title"), rs.getInt("year"), rs.getDouble("rating"),rs.getInt("votes")));
		}

		return movies;
	}
	
	public List<Person> getPeopleByMovieId(int id) throws SQLException{
		List<Person> people = new ArrayList<>();
		String statement = "SELECT * FROM people INNER JOIN stars ON people.id = stars.person_id WHERE stars.movie_id = ?";
		PreparedStatement ps = connection.prepareStatement(statement);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			people.add(new Person(rs.getInt("id"), rs.getString("name"), rs.getString("birth")));
		}
		return people;
	}
}
