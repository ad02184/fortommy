package com.flickfinder.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.flickfinder.util.Database;
import com.flickfinder.model.Movie;
import com.flickfinder.model.Person;
/**
 * TODO: Implement this class
 * 
 */
public class PersonDAO {
	private final Connection connection;
	// for the must have requirements, you will need to implement the following
	// methods:
	// - getAllPeople()
	// - getPersonById(int id)
	// you will add further methods for the more advanced tasks; however, ensure your have completed 
	// the must have requirements before you start these.  
	public PersonDAO() {
		Database database = Database.getInstance();
		connection = database.getConnection();
	}
	
	public List<Person> getAllPeople(String params) throws SQLException {
		String temp = params;
		if (temp == null) {
			temp = "50";
		}
		List<Person> people = new ArrayList<>();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery("select * from people LIMIT "+ temp);
		while (rs.next()) {
			people.add(new Person(rs.getInt("id"), rs.getString("name"), rs.getString("birth")));
		}
		return people;
	}
	
	public List<Person> getAllPeople() throws SQLException {
		return this.getAllPeople(null);
	}
	
	
	public Person getPersonById(int id) throws SQLException{
		String statement = "select * from people where id = ?";
		PreparedStatement ps = connection.prepareStatement(statement);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			return new Person(rs.getInt("id"), rs.getString("name"), rs.getString("birth"));
		}
		return null;
	}
	
	public List<Movie> getMoviesStarringPerson(int id) throws SQLException {
		// Is this finished?
		List<Movie> movies = new ArrayList<>();
		//String statement = "select * from movies where id = (select movie_id from stars where person_id = ?)";
		String statement = "select * from movies inner join stars on movies.id = stars.movie_id where stars.person_id = ?";
		PreparedStatement ps = connection.prepareStatement(statement);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			movies.add(new Movie(rs.getInt("id"), rs.getString("title"), rs.getInt("year")));
		}

		return movies;
	}
}
