package com.flickfinder.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/**
 * 
 * TODO: Implement this class
 * 
 */

class PersonTest {
    private Person person;

    @BeforeEach
	public void setUp() {
		person = new Person(1, "Ali", "2005");
	}

    @Test
	public void testPersonCreated() {
		assertEquals(1, person.getId());
		assertEquals("Ali", person.getName());
		assertEquals("2005", person.getDOB());
	}

    @Test
	public void testPersonSetters() {
		person.setId(3);
		person.setName("Stair");
		person.setDOB("2007");
		assertEquals(3, person.getId());
		assertEquals("Stair", person.getName());
		assertEquals("2007", person.getDOB());
	}
}