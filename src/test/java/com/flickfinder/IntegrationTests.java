package com.flickfinder;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;

import org.hamcrest.collection.HasItemInArray;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.flickfinder.util.Database;
import com.flickfinder.util.Seeder;

import io.javalin.Javalin;

/**
 * These are our integration tests.
 * We are testing the application as a whole, including the database.
 */
class IntegrationTests {

	/**
	 * The Javalin app.*
	 */
	Javalin app;

	/**
	 * The seeder object.
	 */
	Seeder seeder;

	/**
	 * The port number. Try and use a different port number from your main
	 * application.
	 */
	int port = 6000;

	/**
	 * The base URL for our test application.
	 */
	String baseURL = "http://localhost:" + port;

	/**
	 * Bootstraps the application before each test.
	 */
	@BeforeEach
	void setUp() {
		var url = "jdbc:sqlite::memory:";
		seeder = new Seeder(url);
		Database.getInstance(seeder.getConnection());
		app = AppConfig.startServer(port);
	}

	/**
	 * Test that the application retrieves a list of all movies.
	 * Notice how we are checking the actual content of the list.
	 * At this higher level, we are not concerned with the implementation details.
	 */

	@Test
	void retrieves_a_list_of_all_movies() {
		given().when().get(baseURL + "/movies").then().assertThat().statusCode(200). // Assuming a successful
												// response returns HTTP
												// 200
				body("id", hasItems(1, 2, 3, 4, 5))
				.body("title", hasItems("The Shawshank Redemption", "The Godfather",
						"The Godfather: Part II", "The Dark Knight", "12 Angry Men"))
				.body("year", hasItems(1994, 1972, 1974, 2008, 1957));
	}

	@Test
	void retrieves_a_single_movie_by_id() {

		given().when().get(baseURL + "/movies/1").then().assertThat().statusCode(200). // Assuming a successful
												// response returns HTTP
												// 200
				body("id", equalTo(1))
				.body("title", equalTo("The Shawshank Redemption"))
				.body("year", equalTo(1994));
	}

	@Test
	void retrieves_movies_by_person_id() {

		given().when().get(baseURL + "/people/4/movies").then().assertThat().statusCode(200). // Assuming a successful
				body("id", hasItems(2,3))
				.body("title", hasItems("The Godfather", "The Godfather: Part II"))
				.body("year", hasItems(1972, 1974));
	}

	@Test
	void retrieves_movies_by_year_sorted() {
		given().when().get(baseURL + "/movies/ratings/2008").then().assertThat().statusCode(200). // Assuming a successful
				body("id", hasItems(4))
				.body("title", hasItems("The Dark Knight"))
				.body("year", hasItems(2008));
	}

	@Test
	void retrieves_stars_by_movieid() {

		given().when().get(baseURL + "/movies/1/stars").then().assertThat().statusCode(200). // Assuming a successful
												// response returns HTTP
												// 200
				body("id", hasItems(1, 2))
				.body("name", hasItems("Tim Robbins", "Morgan Freeman"))
				.body("dob", hasItems("1958-10-16", "1937-06-01"));
	}

	@Test
	void retrieves_a_list_of_all_people() {
		given().when().get(baseURL + "/people").then().assertThat().statusCode(200). // Assuming a successful
												// response returns HTTP
												// 200
				body("id", hasItems(1, 2, 3, 4, 5))
				.body("name", hasItems("Tim Robbins", "Morgan Freeman",
						"Christopher Nolan", "Al Pacino", "Henry Fonda"))
				.body("dob", hasItems("1958-10-16", "1937-06-01", "1970-07-30", "1940-04-25", "1905-05-16"));
	}

	@Test
	void retrieves_a_single_person_by_id() {

		given().when().get(baseURL + "/people/1").then().assertThat().statusCode(200). // Assuming a successful
				body("id", equalTo(1))
				.body("name", equalTo("Tim Robbins"))
				.body("dob", equalTo("1958-10-16"));
	}
	@Test
	void listofmovieswithlimit(){
		given().when().get(baseURL + "/movies?limit=2").then().assertThat().statusCode(200). // Assuming a successful
				body("id", hasItems(1, 2))
				.body("title", hasItems("The Shawshank Redemption", "The Godfather"))
				.body("year", hasItems(1994, 1972));
	}

	@Test
	void listofallpeoplewithlimit(){
		given().when().get(baseURL + "/people?limit=2").then().assertThat().statusCode(200). // Assuming a successful
				body("id", hasItems(1, 2))
				.body("name", hasItems("Tim Robbins", "Morgan Freeman"))
				.body("dob", hasItems("1958-10-16", "1937-06-01"));
	}

	@Test
	void yearwithlimit(){
		given().when().get(baseURL + "/movies/ratings/1972?limit=1").then().assertThat().statusCode(200). // Assuming a successful
				body("id", hasItems(2))
				.body("title", hasItems("The Godfather"))
				.body("year", hasItems(1972));
	}


	//Should be empty
	@Test
	void yearwithvote(){
		given().when().get(baseURL + "/movies/ratings/1994?votes=2200001").then().assertThat().statusCode(200). // Assuming a successful
				body("id", hasItems())
				.body("title", hasItems())
				.body("year", hasItems());
	}

	/**
	 * Tears down the application after each test.
	 * We want to make sure that each test runs in isolation.
	 */
	@AfterEach
	void tearDown() {
		seeder.closeConnection();
		app.stop();
	}

}
